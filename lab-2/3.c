/*  2017  Piotr Janczyk  */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <math.h>

// "zamiana"
void swap(int* a, int* b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

// "odwroc"
void reverse_array(int* array, int len) {
  for (int i = 0; i < len / 2; i++) {
    swap(&array[i], &array[len - i - 1]);
  }
}

// "wymien_tablice"
void swap_arrays(int* a1, int* a2, int size) {
  for (int i = 0; i < size; i++) {
    swap(&a1[i], &a2[i]);
  }
}

void sort_array(int* data, int size) {
  // selection sort
  for (int i = 0; i < size - 1; i++) {
    int min = i;
    for (int j = i + 1; j < size; j++) {
      if (data[j] < data[min]) {
        min = j;
      }
    }
    swap(&data[i], &data[min]);
  }
}

int one_two(int* a1, int size1, int* a2, int size2, int* result) {
  int i = 0;
  int j = 0;
  int k = 0;

  while (i < size1 || j < size2) {
    if (j >= size2 || (i < size1 && a1[i] <= a2[j])) {
      result[k] = a1[i];
    } else {
      result[k] = a2[j];
    }

    while (i < size1 && a1[i] == result[k]) {
      i++;
    }
    while (j < size2 && a2[j] == result[k]) {
      j++;
    }

    k++;
  }

  return k;
}


void print_array(int* array, int size) {
  printf("[");
  for (int i = 0; i < size; i++) {
    if (i != 0) {
      printf(", ");
    }
    printf("%d", array[i]);
  }
  printf("]\n");
}

void random_fill_array(int* array, int size, int min, int max) {
  for (int i = 0; i < size; i++) {
    int x = rand(); // range [0, RAND_MAX]
    array[i] = min + x % (max - min); // range [min, max]
  }
}


#define N1 5
#define N2 8
#define MAX(a, b) (a > b ? a : b)

int main(void) {
  int a1[N1];
  int a2[N2];

  srand(time(NULL));

  random_fill_array(a1, N1, 1, 10);
  random_fill_array(a2, N2, 1, 10);

  printf("a1: ");
  print_array(a1, N1);
  printf("\n");
  printf("a2: ");
  print_array(a2, N2);
  printf("\n");

  sort_array(a1, N1);
  sort_array(a2, N2);

  printf("sorted a1: ");
  print_array(a1, N1);
  printf("\n");
  printf("sorted a2: ");
  print_array(a2, N2);
  printf("\n");

  int combined[MAX(N1, N2)];

  int combined_size = one_two(a1, N1, a2, N2, combined);

  printf("one_two result: ");
  print_array(combined, combined_size);
  printf("\n");

  return 0;
}

/*  2017  Piotr Janczyk  */

#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/**
 * Print array of doubles
 */
void print_array(double* array, int size) {
  printf("[");
  for (int i = 0; i < size; i++) {
    if (i != 0) {
      printf(", ");
    }
    printf("%f", array[i]);
  }
  printf("]\n");
}

/**
 * Fills array with random real numers from range [min, max]
 */
void random_fill_array(double* array, int size, double min, double max) {
  for (int i = 0; i < size; i++) {
    double x = drand48(); // range [0, 1]
    array[i] = min + x * (max - min); // range [min, max]
  }
}

int main(void) {
  int size;
  scanf("%d", &size);

  double* data = malloc(sizeof(double) * size);

  srand48(time(NULL));
  random_fill_array(data, size, 55, 112);

  int avg_size = (size + 1) / 2;
  double* avg = malloc(sizeof(double) * avg_size);

  for (int i = 0; i < size; i += 2) {
    avg[i / 2] = (data[i] + data[i + 1]) / 2.0;
  }
  if (size % 2 == 1) {
    avg[avg_size - 1] = data[size - 1];
  }

  print_array(data, size);
  print_array(avg, avg_size);

  free(data);
  free(avg);

  return 0;
}

/*  2017  Piotr Janczyk  */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define INPUT_MAX_DIGITS 100
// result of multiplying two 100-digit integers can be at most 200-digit
#define BIGNUM_MAX_DIGITS 200

typedef struct {
  uint8_t digits[BIGNUM_MAX_DIGITS];
  int len;
} bignum_t;

void read_bignum(bignum_t *num) {
  char buf[INPUT_MAX_DIGITS + 1];
  scanf("%100s", buf);
  char* str = buf;
  // skip leading zeros
  while (*str == '0') {
    str++;
  }

  int len = strlen(str);
  for (int i = len - 1; i >= 0; i--) {
    if (str[i] >= '0' && str[i] <= '9') {
      num->digits[len - i - 1] = str[i] - '0';
    } else {
      puts("Error: Invalid number format");
      exit(-1);
    }
  }
  num->len = len;
}

void print_bignum(const bignum_t* num) {
  for (int i = num->len - 1; i >= 0; i--) {
    putchar('0' + num->digits[i]);
  }
}

void multiply_bignum(const bignum_t* a, const bignum_t* b, bignum_t* result) {
  result->len = 0;

  for (int i = 0; i < a->len; i++) {
    for (int j = 0; j < b->len; j++) {
      int pos = i + j;
      if (pos == result->len) {
        result->len++;
        result->digits[pos] = 0;
      }

      result->digits[pos] += a->digits[i] * b->digits[j];

      while (result->digits[pos] > 9) {
        if (pos + 1 == result->len) {
          result->len++;
          result->digits[pos + 1] = 0;
        }

        result->digits[pos + 1] += result->digits[pos] / 10;
        result->digits[pos] %= 10;

        pos++;
      }

    }
  }
}

int main(void) {
  bignum_t a;
  bignum_t b;
  bignum_t result;

  read_bignum(&a);
  read_bignum(&b);

  printf("a = ");
  print_bignum(&a);
  putchar('\n');

  printf("b = ");
  print_bignum(&b);
  putchar('\n');

  multiply_bignum(&a, &b, &result);

  printf("a*b = ");
  print_bignum(&result);
  putchar('\n');

  return 0;
}

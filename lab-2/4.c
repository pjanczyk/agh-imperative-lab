/*  2017  Piotr Janczyk  */

#include <stdio.h>
#include <stdbool.h>

int int_pow(int base, int exp) {
  int result = 1;
  for (int i = 0; i < exp; i++) {
    result *= base;
  }
  return result;
}

bool is_armstrong(int num, int base) {
  int digit_count = 0;

  int tmp = num;
  while (tmp != 0) {
    tmp /= base;
    digit_count++;
  }

  int sum = 0;
  tmp = num;
  while (tmp != 0) {
    int digit = tmp % base;
    tmp /= base;
    sum += int_pow(digit, digit_count);
  }

  return (sum == num);
}


int main(void) {
  int start, end, base;
  printf("start: ");
  scanf("%d", &start);
  printf("end: ");
  scanf("%d", &end);
  printf("base: ");
  scanf("%d", &base);

  for (int i = start; i <= end; i++) {
    if (is_armstrong(i, base)) {
      printf("%d\n", i);
    }
  }
}

/* 2017  Piotr Janczyk */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  char brand[20]; // marka samochodu
  int engine_number; // numer silnika
  char model[20]; // model samochodu
} car;

// Funkcja porownujaca samochody wedlug marek
int car_by_brand_comparator(const void* p1, const void* p2) {
  const car* c1 = (const car*)p1;
  const car* c2 = (const car*)p2;
  return strcmp(c1->brand, c2->brand);
}

// Zlicza liczbe wszystkich marek w posortowanej tablicy samochodow
int count_brands(car* cars, int n) {
  char* prev_brand = NULL;
  int count = 0;

  for (int i = 0; i < n; i++) {
    if (prev_brand == NULL || strcmp(cars[i].brand, prev_brand) != 0) {
      prev_brand = cars[i].brand;
      count++;
    }
  }

  return count;
}

// Wypelnia tablice `array_2` oraz `lenw` danymi
void fill_array_2(car* cars, int n, car** array_2, int* lenw, int lw) {
  for (int i = 0; i < lw; i++) {
    lenw[i] = 0;
  }

  char* prev_brand = NULL;
  int brand_idx = -1;

  for (int car_idx = 0; car_idx < n; car_idx++) {
    if (prev_brand == NULL || strcmp(cars[car_idx].brand, prev_brand) != 0) {
      brand_idx++;
      array_2[brand_idx] = &cars[car_idx];
    }
    lenw[brand_idx]++;
    prev_brand = cars[car_idx].brand;
  }
}

// Znajduje indeks pod jakim wyszukiwana marka znajduje sie w tablicy `array_2`
int find_brand(char* search, car** array_2, int lw) {
  for (int i = 0; i < lw; i++) {
    if (strcmp(array_2[i][0].brand, search) == 0) {
      return i;
    }
  }
  return -1;
}


int main(void) {
  int n;
  scanf("%d", &n);

  car* cars = malloc(n * sizeof(car));

  for (int i = 0; i < n; i++) {
    scanf("%19s%d%19s",
          cars[i].brand,
          &cars[i].engine_number,
          cars[i].model);
  }

  char brand_search[20];
  scanf("%19s", brand_search);

  // posorowanie tablicy `cars`
  qsort(cars, n, sizeof(car), car_by_brand_comparator);

  // zliczenie ilosci marek
  int lw = count_brands(cars, n);

  car** array_2 = malloc(lw * sizeof(car*));
  int* lenw = malloc(lw * sizeof(int));

  // wypelnienie tablic `array_2` oraz `lenw`
  fill_array_2(cars, n, array_2, lenw, lw);

  // znalezienie indeksu wyszukiwanej marki
  int brand_idx = find_brand(brand_search, array_2, lw);

  // wypisanie wynikow wyszukiwania
  if (brand_idx == -1) {
    printf("Nie znaleziono marki: %s\n", brand_search);
  } else {
    for (int i = 0; i < lenw[brand_idx]; i++) {
      car* car = &array_2[brand_idx][i];
      printf("marka: %s, numer silnika: %d, model: %s\n",
             car->brand,
             car->engine_number,
             car->model);
    }
  }

  return 0;
}

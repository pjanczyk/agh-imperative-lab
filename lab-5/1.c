/* 2017  Piotr Janczyk */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct {
  double re;
  double im;
} complex_t;

double random_double(double min, double max) {
  double a = (double)rand() / (double) RAND_MAX; // a in range [0.0, 1.0]
  a = (max - min) * a + min; // a in range [min, max]
  return a;
}

double complex_magnitude(const complex_t* c) {
  return sqrt(c->re * c->re + c->im * c->im);
}

int compare_complex_by_magnitude(const void* c1, const void* c2) {
  double magnitude1 = complex_magnitude((const complex_t*) c1);
  double magnitude2 = complex_magnitude((const complex_t*) c2);

  if (magnitude1 < magnitude2) {
    return -1;
  } else if (magnitude1 == magnitude2) {
    return 0;
  } else {
    return 1;
  }
}

int main() {
  srand(time(NULL));

  int n;
  scanf("%d", &n);

  complex_t* data = malloc(n * sizeof(complex_t));

  for (int i = 0; i < n; i++) {
    data[i].re = random_double(-10.0, 10.0);
    data[i].im = random_double(-10.0, 10.0);
  }

  qsort(data, n, sizeof(complex_t), &compare_complex_by_magnitude);

  printf("Posortowane:\n");

  for (int i = 0; i < n; i++) {
    printf("(% 2.2f, % 2.2f)  modul: %2.2f\n",
      data[i].re,
      data[i].im,
      complex_magnitude(&data[i]));
  }

  return 0;
}

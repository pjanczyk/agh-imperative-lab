/* 2017  Piotr Janczyk */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

struct node {
  struct node* prev;
  struct node* next;
  int value;
};
typedef struct node node_t;

typedef struct {
  node_t* first;
  node_t* last;
} list_t;

// forward declarations
list_t make_list();
int empty(const list_t* list);
int size(const list_t* list);
void clear(list_t* list);
void push_front(list_t* list, int val);
void push_back(list_t* list, int val);
void remove_val(list_t* list, int val);
void pop_front(list_t* list);
void pop_back(list_t* list);
int front(const list_t* list);
int back(const list_t* list);
void print(const list_t* list);

// implementations

// Creates an empty list
list_t make_list() {
  list_t result = { .first = NULL, .last = NULL };
  return result;
}

int empty(const list_t* list) {
   if (list->first == NULL) {
     return 1;
   } else {
     return 0;
   }
}

int size(const list_t* list) {
  int s = 0;
  node_t* p = list->first;
  while (p != NULL) {
    s++;
    p = p->next;
  }
  return s;
}

void clear(list_t* list) {
  while (!empty(list)) {
    pop_front(list);
  }
}

void push_front(list_t* list, int val) {
  node_t* node = malloc(sizeof(node_t));
  node->value = val;
  node->prev = NULL;
  if (empty(list)) {
    node->next = NULL;
    list->first = node;
    list->last = node;
  } else {
    node->next = list->first;
    list->first->prev = node;
    list->first = node;
  }
}

void push_back(list_t* list, int val) {
  node_t* node = malloc(sizeof(node_t));
  node->value = val;
  node->next = NULL;
  if (empty(list)) {
    node->prev = NULL;
    list->first = node;
    list->last = node;
  } else {
    node->prev = list->last;
    list->last->next = node;
    list->last = node;
  }
}

void remove_node(list_t* list, node_t* node) {
  if (node == list->first) {
    pop_front(list);
  } else if (node == list->last) {
    pop_back(list);
  } else {
    node->prev->next = node->next;
    node->next->prev = node->prev;
    free(node);
  }
}

void remove_val(list_t* list, int val) {
  node_t* p = list->first;
  while (p != NULL) {
    node_t* tmp = p;
    p = p->next;
    if (tmp->value == val) {
      remove_node(list, tmp);
    }
  }
}

void pop_front(list_t* list) {
  assert(!empty(list));

  node_t* tmp = list->first;
  list->first = tmp->next;
  if (list->first != NULL) {
    list->first->prev = NULL;
  }
  free(tmp);
}

void pop_back(list_t* list) {
  assert(!empty(list));

  node_t* tmp = list->last;
  list->last = tmp->prev;
  if (list->last != NULL) {
    list->last->next = NULL;
  }
  free(tmp);
}

int front(const list_t* list) {
  assert(!empty(list));
  return list->first->value;
}

int back(const list_t* list) {
  assert(!empty(list));
  return list->last->value;
}

void print(const list_t* list) {
  printf("[ ");
  node_t* p = list->first;
  while (p != NULL) {
    printf("%d ", p->value);
    p = p->next;
  }
  printf("]\n");
}

// Marco that prints and executes command
#define PRINT_COMMAND(a) (puts(#a), a)

int main() {
  // some tests
  list_t list = make_list();
  print(&list);
  PRINT_COMMAND(push_back(&list, 1));
  print(&list);
  PRINT_COMMAND(push_back(&list, 2));
  print(&list);
  PRINT_COMMAND(push_back(&list, 2));
  print(&list);
  PRINT_COMMAND(push_back(&list, 3));
  print(&list);
  PRINT_COMMAND(push_back(&list, 4));
  print(&list);
  PRINT_COMMAND(push_front(&list, 0));
  print(&list);
  PRINT_COMMAND(pop_back(&list));
  print(&list);
  PRINT_COMMAND(pop_front(&list));
  print(&list);
  PRINT_COMMAND(remove_val(&list, 2));
  print(&list);

  int s = PRINT_COMMAND(size(&list)); printf("%d\n", s);
  int f = PRINT_COMMAND(front(&list)); printf("%d\n", f);
  int b = PRINT_COMMAND(back(&list)); printf("%d\n", b);

  return 0;
}

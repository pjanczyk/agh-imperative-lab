/* 2017  Piotr Janczyk */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* DIGIT_MAP[3][10] = {
  {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"}, // cyfry jednosci
  {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"}, // cyfry dziesiatek
  {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"}  // cyfry setek
};

/**
 * Konwertuje liczbe w systemie rzymskim do INTa.
 * Jesli napis wejsciowy nie jest poprawna liczba w systemie rzymskim,
 * zwraca -1.
 */
int roman_to_int(const char* roman) {
  int result = 0;

  // cyfry tysiecy
  while (*roman == 'M') {
    result += 1000;
    roman++;
  }

  // cyfry setek, dziesiatek i jednosci
  int multiplier = 100;
  for (int k = 2; k >= 0; k--) {
    for (int d = 9; d >= 1; d--) {
      const char* dig = DIGIT_MAP[k][d];
      int len = strlen(dig);

      if (strncmp(roman, dig, len) == 0) {
        roman += len;
        result += multiplier * d;
        break;
      }
    }

    multiplier /= 10;
  }

  if (*roman == '\0') {
    return result;
  } else {
    // jesli nie doszlismy do konca napisu, to znaczy, ze format jest niepoprawny
    return -1;
  }
}

/**
 * Konwertuje INTa do liczby w systemie rzymskim
 */
void int_to_roman(int n, char* buf) {
  int digits[4] = {
    n % 10, // cyfra jednosci
    (n / 10) % 10, // cyfra dziesiatek
    (n / 100) % 10, // cyfra setek
    n / 1000 // cyfra tysiecy
  };

  // cyfra tysiecy
  int i;
  for (i = 0; i < digits[3]; i++) {
    buf[i] = 'M';
  }
  buf[i] = '\0';

  // cyfra setek, dziesiatek, jednosci
  for (int k = 2; k >= 0; k--) {
    int d = digits[k];
    strcat(buf, DIGIT_MAP[k][d]);
  }
}

int main() {
  char a_roman[100];
  char b_roman[100];
  char sum_roman[200];

  scanf("%99s %99s", a_roman, b_roman);

  int a = roman_to_int(a_roman);
  int b = roman_to_int(b_roman);

  // Obsluga niepoprawnych danych wejsciowych
  if (a == -1) {
    printf("Niepoprawna liczba w systemie rzymskim: %s\n", a_roman);
    return 1;
  }
  if (b == -1) {
    printf("Niepoprawna liczba w systemie rzymskim: %s\n", b_roman);
    return 1;
  }

  int sum = a + b;
  int_to_roman(sum, sum_roman);

  printf("%s\n", sum_roman);

  return 0;
}

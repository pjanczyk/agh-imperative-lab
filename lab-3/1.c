/*  2017  Piotr Janczyk  */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SZ 10

void init(int arr[SZ][SZ]) {
  for (int i = 0; i < SZ; i++) {
    for (int j = 0; j < SZ; j++) {
      arr[i][j] = (rand() % 33) + 12;
    }
  }
}

int diagonal_sum(int arr[SZ][SZ]) {
  int sum = 0;
  for (int i = 0; i < SZ; i++) {
    sum += arr[i][i];
  }
  return sum;
}

int below_diagonal_sum(int arr[SZ][SZ]) {
  int sum = 0;
  for (int i = 0; i < SZ; i++) {
    for (int j = SZ - 1 - i; j < SZ; j++) {
        sum += arr[i][j];
    }
  }
  return sum;
}

int whole_sum(int arr[SZ][SZ]) {
  int sum = 0;
  for (int i = 0; i < SZ; i++) {
    for (int j = 0; j < SZ; j++) {
      sum += arr[i][j];
    }
  }
  return sum;
}

void multiply(int a[SZ][SZ], int b[SZ][SZ], int res[SZ][SZ]) {
  for (int i = 0; i < SZ; i++) {
    for (int j = 0; j < SZ; j++) {
      res[i][j] = 0;

      for (int k = 0; k < SZ; k++) {
        res[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

void print(int arr[SZ][SZ]) {
  for (int i = 0; i < SZ; i++) {
    for (int j = 0; j < SZ; j++) {
      printf("%d ", arr[i][j]);
    }
    printf("\n");
  }
}

int main() {
  srand(time(NULL));

  int a[SZ][SZ];
  int b[SZ][SZ];

  init(a);
  init(b);

  printf("tablica A:\n");
  print(a);
  printf("\n");

  printf("suma elementow na przekatnej A: %d\n", diagonal_sum(a));
  printf("suma elementow pod przekatna A: %d\n", below_diagonal_sum(a));
  printf("suma wszystkich elementow A: %d\n", whole_sum(a));

  printf("\n\ntablica B:\n");
  print(b);

  int res[SZ][SZ];

  printf("\n\nmnozenie A*B:\n");
  multiply(a, b, res);
  print(res);

  return 0;
}

/*  2017  Piotr Janczyk  */
#include <stdio.h>
#include <stdlib.h>

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))


void print_table(int* table, int size) {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      printf("%d ", table[i * size + j]);
    }
    printf("\n");
  }
}

int main() {
  int n, r;
  scanf("%d%d", &n, &r);

  int* input = malloc(sizeof(int) * n * n);

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      scanf("%d", &input[i * n + j]);
    }
  }

  int* result = malloc(sizeof(int) * n * n);

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      result[i * n + j] = 0;

      for (int y = MAX(i - r, 0); y <= MIN(i + r, n - 1); y++) {
        for (int x = MAX(j - r, 0); x <= MIN(j + r, n - 1); x++) {
          result[i * n + j] += input[y * n + x];
        }
      }
    }
  }

  print_table(result, n);

  free(input);
  free(result);

  return 0;

}

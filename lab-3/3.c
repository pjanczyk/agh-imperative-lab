/*  2017  Piotr Janczyk  */

// Program rozwiazujacy uklad rownan metoda eliminacji Gaussa

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

// maksymalny rozmiar układu
#define SZ 100
// liczby mniejsze od 10^-6 beda uznawane za zero
#define EPSILON 1e-6

typedef enum {
  NO_SOLUTIONS,
  UNIQUE_SOLUTION,
  INFINITELY_MANY_SOLUTIONS
} solution_number_t;

typedef struct {
  int n;
  double mat[SZ][SZ]; // array of size n x (n + 1)
  int row_to_col_map[SZ]; // array of size n
  solution_number_t solution_number;
  double unique_solution[SZ]; // array of size n
} data_t;

void debug_print(const data_t* data) {
  for (int row = 0; row < data->n; row++) {
    for (int col = 0; col < data->n; col++) {
      printf("%6.2f ", data->mat[row][col]);
    }
    printf("| %6.2f\n", data->mat[row][data->n]);
  }
  printf("\n");
}

// Od wiersza target_row odejmuje wiersz base_row pomnozony przez wspolczynnik,
// tak aby liczba w wierszu target_row w kolumnie base_col byla rowna 0.0
void substract_rows(data_t* data, int base_row, int target_row, int base_col) {
    double f = data->mat[target_row][base_col] / data->mat[base_row][base_col];

    for (int c = 0; c <= data->n; c++) {
        data->mat[target_row][c] -= f * data->mat[base_row][c];
    }
    data->mat[target_row][base_col] = 0.0;

    printf("w%d += %6.2f * w%d\n", target_row + 1, -f, base_row + 1);
    debug_print(data);
}

int find_non_zero_row(const data_t* data, int base_col) {
  for (int row = 0; row < data->n; row++) {
    if (data->mat[row][base_col] != 0.0 && data->row_to_col_map[row] == -1) {
      return row;
    }
  }
  return -1;
}

void load(data_t* data) {
  scanf("%d", &data->n);

  // wczytaj współczynniki A
  for (int row = 0; row < data->n; row++) {
    for (int col = 0; col < data->n; col++) {
      scanf("%lf", &data->mat[row][col]);
    }
  }

  // wczytaj wyrazy wolne
  for (int row = 0; row < data->n; row++) {
    scanf("%lf", &data->mat[row][data->n]);
  }

  for (int i = 0; i < data->n; i++) {
      data->row_to_col_map[i] = -1;
  }
}

void solve(data_t* data) {
  printf("Rozwiazywanie ukladu:\n");
  debug_print(data);

  for (int col = 0; col < data->n; col++) {
    int base_row = find_non_zero_row(data, col);
    if (base_row == -1) continue; // kolumna c jest wypelniona samymi zerami

    data->row_to_col_map[base_row] = col;

    for (int row = 0; row < data->n; row++) {
      if (row != base_row) {
        substract_rows(data, base_row, row, col); // odejmij wiersz base_row od wiersza r
      }
    }

  }

  bool has_contradition = false;
  bool has_identity = false;

  for (int row = 0; row < data->n; row++) {
    if (data->row_to_col_map[row] == -1) {
      if (fabs(data->mat[row][data->n]) < EPSILON) {
        has_identity = true; // rownanie tozsamosciowe
      } else {
        has_contradition = true; // rownanie sprzeczne
      }
    }
  }

  if (has_contradition) {
    data->solution_number = NO_SOLUTIONS;
    return;
  }

  if (has_identity) {
    data->solution_number = INFINITELY_MANY_SOLUTIONS;
    return;
  }

  data->solution_number = UNIQUE_SOLUTION;
  for (int row = 0; row < data->n; row++) {
    int col = data->row_to_col_map[row];
    data->unique_solution[col] = data->mat[row][data->n] / data->mat[row][col];
  }
}

int main() {
  data_t data;
  load(&data);
  solve(&data);

  switch (data.solution_number) {
    case NO_SOLUTIONS:
      printf("Brak rozwiazan\n");
      break;

    case INFINITELY_MANY_SOLUTIONS:
      printf("Nieskonczenie wiele rozwiazan\n");
      break;

    case UNIQUE_SOLUTION:
      printf("Dokladnie jedno rozwiazanie:\n");
      for (int i = 0; i < data.n; i++) {
        printf("x%d = %6.2f\n", i + 1, data.unique_solution[i]);
      }
  }

  return 0;
}
